@shift /0
@ECHO OFF
TITLE ADB and Fastboot Installer Ver.G36
START /wait uncab.exe
COLOR 0C
@ECHO.                    System Information
@ECHO.
@ECHO.      *Username                :%USERNAME%
@ECHO.      *Computer Name           :%COMPUTERNAME%
@ECHO.      *Processor Architecture  :%PROCESSOR_ARCHITECTURE%
@ECHO.      *Processor Identifier    :%PROCESSOR_IDENTIFIER%
@ECHO.      *Processor Level         :%PROCESSOR_LEVEL%
@ECHO.      *Processor Reversion     :%PROCESSOR_REVISION%
@ECHO.      *Number of Processor     :%NUMBER_OF_PROCESSORS%
PING localhost -n 3 >nul
COLOR 0A
ECHO ###############################################################
ECHO #                                                             #
ECHO #     Android Debuging Bridge Tools Revision G36 By KKSiS     #
ECHO #                                                             #
ECHO #      [Package details]                                      #
ECHO #          - [Android Debug Bridge version 1.0.36]            #
ECHO #          - [Google USB Driver revision 11]                  #
ECHO #          - [Fastboot Tools revision 24]                     #
ECHO #                                                             #
ECHO #                                                             #
ECHO #                      IMPORTANT NOTICE                       #
ECHO #               *****************************                 #
ECHO #               # RUN AS ADMINISTRATOR ONLY #                 #
ECHO #               *****************************                 #
ECHO #                                                             #
ECHO ###############################################################
ECHO.
ECHO.
ECHO.
ECHO.

:Q1
ECHO(
SET /P ANSWER=Do you want to install ADB and Fastboot? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO Q2)
 IF /i {%ANSWER%}=={yes} (GOTO Q2)
 IF /i {%ANSWER%}=={n} (GOTO DRV)
 IF /i {%ANSWER%}=={no} (GOTO DRV)
ECHO(
ECHO Bad answer! Use only Y/N or Yes/No
GOTO Q1

:Q2
ECHO(
SET /P ANSWER=Install ADB system-wide? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO ADB_S)
 IF /i {%ANSWER%}=={yes} (GOTO ADB_S)
 IF /i {%ANSWER%}=={n} (GOTO ADB_U)
 IF /i {%ANSWER%}=={no} (GOTO ADB_U)
ECHO(
ECHO Bad answer! Use only Y/N or Yes/No
GOTO Q2

:ADB_U
ECHO(
ECHO Installing ADB and Fastboot ... (current user only)
ECHO(
SET /a FCnt=0
IF EXIST %WINDIR%\adb.exe DEL %WINDIR%\adb.exe /f /q
IF EXIST %WINDIR%\AdbWinApi.dll DEL %WINDIR%\AdbWinApi.dll /f /q
IF EXIST %WINDIR%\AdbWinUsbApi.dll DEL %WINDIR%\AdbWinUsbApi.dll /f /q
IF EXIST %WINDIR%\fastboot.exe DEL %WINDIR%\fastboot.exe /f /q
IF EXIST %ProgramData%\adb.exe DEL %ProgramData%\adb.exe /f /q
IF EXIST %ProgramData%\AdbWinApi.dll DEL %ProgramData%\AdbWinApi.dll /f /q
IF EXIST %ProgramData%\AdbWinUsbApi.dll DEL %ProgramData%\AdbWinUsbApi.dll /f /q
IF EXIST %ProgramData%\fastboot.exe DEL %ProgramData%\fastboot.exe /f /q
IF EXIST %SYSTEMDRIVE%\adb.exe DEL %SYSTEMDRIVE%\adb.exe /f /q
IF EXIST %SYSTEMDRIVE%\AdbWinApi.dll DEL %SYSTEMDRIVE%\AdbWinApi.dll /f /q
IF EXIST %SYSTEMDRIVE%\AdbWinUsbApi.dll DEL %SYSTEMDRIVE%\AdbWinUsbApi.dll /f /q
IF EXIST %SYSTEMDRIVE%\fastboot.exe DEL %SYSTEMDRIVE%\fastboot.exe /f /q
IF EXIST %USERPROFILE%\adb.exe DEL %USERPROFILE%\adb.exe /f /q
IF EXIST %USERPROFILE%\AdbWinApi.dll DEL %USERPROFILE%\AdbWinApi.dll /f /q
IF EXIST %USERPROFILE%\AdbWinUsbApi.dll DEL %USERPROFILE%\AdbWinUsbApi.dll /f /q
IF EXIST %USERPROFILE%\fastboot.exe DEL %USERPROFILE%\fastboot.exe /f /q
FC /b data\adb.exe %USERPROFILE%\adb\adb.exe >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\adb.exe %USERPROFILE%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\AdbWinApi.dll %USERPROFILE%\adb\AdbWinApi.dll >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\AdbWinApi.dll %USERPROFILE%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\AdbWinUsbApi.dll %USERPROFILE%\adb\AdbWinUsbApi.dll >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\AdbWinUsbApi.dll %USERPROFILE%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\fastboot.exe %USERPROFILE%\adb\fastboot.exe >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\fastboot.exe %USERPROFILE%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
IF %FCnt% GTR 3 ECHO All %FCnt% files copied. && GOTO PATH_U
IF %FCnt% GTR 1 ECHO Just %FCnt% files copied. && GOTO PATH_U
IF %FCnt% LSS 1 ECHO Nothing need to be copied. && GOTO PATH_U
ECHO Just %FCnt% file copied.
GOTO PATH_U

:PATH_U
ECHO %PATH% > PATH.TMP
ver > nul
FIND "%USERPROFILE%\adb" PATH.TMP > nul 2>&1
IF %ERRORLEVEL% LSS 1 GOTO DRVU
DEL PATH.TMP
SETX PATH "%PATH%;%USERPROFILE%\adb"
GOTO DRVU

:ADB_S
ECHO(
ECHO Installing ADB and Fastboot ... (system-wide)
ECHO(
SET /a FCnt=0
IF EXIST %WINDIR%\adb.exe DEL %WINDIR%\adb.exe /f /q
IF EXIST %WINDIR%\AdbWinApi.dll DEL %WINDIR%\AdbWinApi.dll /f /q
IF EXIST %WINDIR%\AdbWinUsbApi.dll DEL %WINDIR%\AdbWinUsbApi.dll /f /q
IF EXIST %WINDIR%\fastboot.exe DEL %WINDIR%\fastboot.exe /f /q
IF EXIST %ProgramData%\adb.exe DEL %ProgramData%\adb.exe /f /q
IF EXIST %ProgramData%\AdbWinApi.dll DEL %ProgramData%\AdbWinApi.dll /f /q
IF EXIST %ProgramData%\AdbWinUsbApi.dll DEL %ProgramData%\AdbWinUsbApi.dll /f /q
IF EXIST %ProgramData%\fastboot.exe DEL %ProgramData%\fastboot.exe /f /q
IF EXIST %SYSTEMDRIVE%\adb.exe DEL %SYSTEMDRIVE%\adb.exe /f /q
IF EXIST %SYSTEMDRIVE%\AdbWinApi.dll DEL %SYSTEMDRIVE%\AdbWinApi.dll /f /q
IF EXIST %SYSTEMDRIVE%\AdbWinUsbApi.dll DEL %SYSTEMDRIVE%\AdbWinUsbApi.dll /f /q
IF EXIST %SYSTEMDRIVE%\fastboot.exe DEL %SYSTEMDRIVE%\fastboot.exe /f /q
IF EXIST %USERPROFILE%\adb.exe DEL %USERPROFILE%\adb.exe /f /q
IF EXIST %USERPROFILE%\AdbWinApi.dll DEL %USERPROFILE%\AdbWinApi.dll /f /q
IF EXIST %USERPROFILE%\AdbWinUsbApi.dll DEL %USERPROFILE%\AdbWinUsbApi.dll /f /q
IF EXIST %USERPROFILE%\fastboot.exe DEL %USERPROFILE%\fastboot.exe /f /q
FC /b data\adb.exe %ProgramData%\adb\adb.exe >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\adb.exe %ProgramData%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\AdbWinApi.dll %ProgramData%\adb\AdbWinApi.dll >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\AdbWinApi.dll %ProgramData%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\AdbWinUsbApi.dll %ProgramData%\adb\AdbWinUsbApi.dll >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\AdbWinUsbApi.dll %ProgramData%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
FC /b data\fastboot.exe %ProgramData%\adb\fastboot.exe >nul 2>&1
IF %ERRORLEVEL% GTR 0 xcopy adb\fastboot.exe %ProgramData%\adb\ /y /q >nul 2>&1 && SET /a FCnt=%FCnt%+1
IF %FCnt% GTR 3 ECHO All %FCnt% files copied. && GOTO PATH_S
IF %FCnt% GTR 1 ECHO Just %FCnt% files copied. && GOTO PATH_S
IF %FCnt% LSS 1 ECHO Nothing need to be copied. && GOTO PATH_S
ECHO Just %FCnt% file copied.
GOTO PATH_S

:PATH_S
ECHO %PATH% > PATH.TMP
ver > nul
FIND "%ProgramData%\adb" PATH.TMP > nul 2>&1
IF %ERRORLEVEL% LSS 1 GOTO DRVS
DEL PATH.TMP
SETX PATH "%PATH%;%ProgramData%\adb" /m
GOTO DRVS

:DRV
ECHO(
SET /P ANSWER=Do you want to install device drivers? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO DRIVER)
 IF /i {%ANSWER%}=={yes} (GOTO DRIVER)
 IF /i {%ANSWER%}=={n} (GOTO FINISH)
 IF /i {%ANSWER%}=={no} (GOTO FINISH)
ECHO Bad answer! Use only Y/N or Yes/No
GOTO DRV

:DRVU
ECHO(
SET /P ANSWER=Do you want to install device drivers? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO STU)
 IF /i {%ANSWER%}=={yes} (GOTO STU)
 IF /i {%ANSWER%}=={n} (GOTO FINISH)
 IF /i {%ANSWER%}=={no} (GOTO FINISH)
ECHO Bad answer! Use only Y/N or Yes/No
GOTO DRVU

:DRVS
ECHO(
SET /P ANSWER=Do you want to install device drivers? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO STU)
 IF /i {%ANSWER%}=={yes} (GOTO STU)
 IF /i {%ANSWER%}=={n} (GOTO FINISH)
 IF /i {%ANSWER%}=={no} (GOTO FINISH)
ECHO Bad answer! Use only Y/N or Yes/No
GOTO DRVS

:STU
SET /P ANSWER=Do you want to Run ADB Service on Windows Startup? (Y/N)
 IF /i {%ANSWER%}=={y} (GOTO STUC)
 IF /i {%ANSWER%}=={yes} (GOTO STUC)
 IF /i {%ANSWER%}=={n} (GOTO FINISH)
 IF /i {%ANSWER%}=={no} (GOTO FINISH)
ECHO Bad answer! Use only Y/N or Yes/No
GOTO STU

:STUC
ECHO(
ECHO Installing Services ...
xcopy data\STUC.exe %ProgramData%\Microsoft\Windows\Start Menu\Programs\Startup\ /y /q >nul
ECHO Service Has Installed!!
GOTO DRIVER

:DRIVER
IF DEFINED programfiles(x86) GOTO x64

:x86
ECHO(
ECHO Installing 32-bit driver ...
PING localhost -n 2 >NUL
START data\DPInst_x86 /f
GOTO FINISH

:x64
ECHO(
ECHO Installing 64-bit driver ...
PING localhost -n 2 >NUL
START data\DPInst_x64 /f
GOTO FINISH

:FINISH
ECHO(
ECHO All done!
PING localhost -n 3 >NUL
EXIT
